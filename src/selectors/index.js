import * as R from 'ramda'

export const getOvenById = (state, id) => R.prop(id, state.ovens)

export const getActiveCategoryId = ownProps =>
  R.path(['match', 'params', 'id'], ownProps)

export const getOvens = (state, ownProps) => {
  const activeCategoryId = getActiveCategoryId(ownProps)

  const applySearch = item =>
    R.contains(
      R.toLower(state.ovensPage.search),
      R.compose(R.toLower, R.prop('name'))(item)
    )

  const applyCategory = item =>
    R.equals(activeCategoryId, R.prop('categoryId', item))

  return R.compose(
    R.filter(applySearch),
    R.when(R.always(activeCategoryId), R.filter(applyCategory)),
    R.map(id => getOvenById(state, id))
  )(state.ovensPage.ids)
}

//Количество товаров показанных на странице
export const getRenderedOvensLength = state => R.length(state.ovensPage.ids)

export const getTotalBasketCount = state => R.length(state.basket)

export const getTotalBasketPrice = state =>
  R.compose(R.sum, R.pluck('price'), R.map(id => getOvenById(state, id)))(
    state.basket
  )

export const getBasketOvensWithCount = state => {
  const uniqueIds = R.uniq(state.basket)

  const ovenCount = id =>
    R.compose(R.length, R.filter(basketId => R.equals(id, basketId)))(
      state.basket
    )

  const ovenWithCount = oven => R.assoc('count', ovenCount(oven.id), oven)

  return R.compose(R.map(ovenWithCount), R.map(id => getOvenById(state, id)))(
    uniqueIds
  )
}

export const getCategories = state => R.values(state.categories)

export const getSearch = state => state.ovensPage.search
