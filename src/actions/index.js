import {
  FETCH_OVENS_START,
  FETCH_OVENS_SUCCESS,
  FETCH_OVENS_FAILURE,
  FETCH_CATEGORIES_START,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAILURE,
  LOAD_MORE_OVENS_START,
  LOAD_MORE_OVENS_SUCCESS,
  LOAD_MORE_OVENS_FAILURE,
  FETCH_OVEN_BY_ID_START,
  FETCH_OVEN_BY_ID_SUCCESS,
  FETCH_OVEN_BY_ID_FAILURE,
  ADD_OVEN_TO_BASKET,
  REMOVE_OVEN_FROM_BASKET,
  REMOVE_ONE_OVEN_FROM_BASKET,
  CLEAN_BASKET,
  SEARCH_OVEN
} from '../constants'

import {
  fetchOvens as fetchOvensApi,
  fetchOvenById as fetchOvenByIdApi,
  fetchCategories as fetchCategoriesApi
} from 'api'
import { getRenderedOvensLength } from 'selectors'

export const fetchOvens = () => async dispatch => {
  dispatch({ type: FETCH_OVENS_START })

  try {
    const ovens = await fetchOvensApi()

    setTimeout(() => {
      dispatch({
        type: FETCH_OVENS_SUCCESS,
        payload: ovens
      })
    }, 3000)
  } catch (error) {
    dispatch({
      type: FETCH_OVENS_FAILURE,
      payload: error,
      error: true
    })
  }
}

export const fetchCategories = () => async dispatch => {
  dispatch({ type: FETCH_CATEGORIES_START })

  try {
    const categories = await fetchCategoriesApi()

    dispatch({
      type: FETCH_CATEGORIES_SUCCESS,
      payload: categories
    })
  } catch (error) {
    dispatch({
      type: FETCH_CATEGORIES_FAILURE,
      payload: error,
      error: true
    })
  }
}

export const loadMoreOvens = () => async (dispatch, getState) => {
  const loadedLength = getRenderedOvensLength(getState())

  dispatch({ type: LOAD_MORE_OVENS_START })

  try {
    const ovens = await fetchOvensApi(loadedLength)

    dispatch({ type: LOAD_MORE_OVENS_SUCCESS, payload: ovens })
  } catch (error) {
    dispatch({
      type: LOAD_MORE_OVENS_FAILURE,
      payload: error,
      error: true
    })
  }
}

export const fetchOvenById = id => async dispatch => {
  dispatch({ type: FETCH_OVEN_BY_ID_START })

  try {
    const oven = await fetchOvenByIdApi(id)

    dispatch({ type: FETCH_OVEN_BY_ID_SUCCESS, payload: oven })
  } catch (error) {
    dispatch({
      type: FETCH_OVEN_BY_ID_FAILURE,
      payload: error,
      error: true
    })
  }
}

export const searchOven = text => dispatch => {
  dispatch({ type: SEARCH_OVEN, payload: text })
}

export const addOvenToBasket = id => dispatch => {
  dispatch({ type: ADD_OVEN_TO_BASKET, payload: id })
}

export const removeOvenFromBasket = id => dispatch => {
  dispatch({ type: REMOVE_OVEN_FROM_BASKET, payload: id })
}

export const removeOneOvenFromBasket = id => dispatch => {
  dispatch({ type: REMOVE_ONE_OVEN_FROM_BASKET, payload: id })
}

export const cleanBasket = () => dispatch => {
  dispatch({ type: CLEAN_BASKET })
}

export const basketCheckout = ovens => () => {
  //alert(JSON.stringify(ovens))

  let result = 'Вы заказали:\n'
  ovens.forEach(oven => (result += `${oven.name} - ${oven.count} шт. \n`))
  result += 'Спасибо за заказ. Мы Вас найдем. )'
  alert(result)
}
