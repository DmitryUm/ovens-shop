import React from 'react'
import { Link } from 'react-router-dom'
import { Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'
import { getTotalBasketCount, getTotalBasketPrice } from 'selectors'

const BasketCart = ({ totalBasketCount, totalPrice }) => {
  const getSuffix = count => {
    const lastDigit = +String(count).split('')[String(count).length - 1]

    if (count > 4 && count < 21) return 'ов'
    if (lastDigit === 1) return ''
    if (lastDigit > 1 && lastDigit < 5) return 'а'

    return 'ов'
  }

  const suffix = getSuffix(totalBasketCount)

  return (
    <Link to="/basket" id="dLabel" className="btn btn-info btn-block">
      <Glyphicon glyph="shopping-cart" style={{ marginRight: '10px' }} />
      {totalBasketCount} товар{suffix} - {totalPrice} руб.
    </Link>
  )
}

const mapStateToProps = state => ({
  totalBasketCount: getTotalBasketCount(state),
  totalPrice: getTotalBasketPrice(state)
})

export default connect(mapStateToProps, null)(BasketCart)
