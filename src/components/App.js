import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { Grid } from 'react-bootstrap'

import { Ovens, Oven, Basket } from 'components'

class App extends Component {
  render() {
    return (
      <div className="view-container">
        <Grid>
          <Route exact path="/" component={Ovens} />
          <Route path="/categories/:id" component={Ovens} />
          <Route path="/oven/:id" component={Oven} />
          <Route path="/basket" component={Basket} />
        </Grid>
      </div>
    )
  }
}

export default App
