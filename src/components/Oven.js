import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col, Thumbnail, Button, Image, Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import * as R from 'ramda'

import { fetchOvenById, addOvenToBasket } from 'actions'
import { getOvenById } from 'selectors'

import { BasketCart } from 'components'

class Oven extends Component {
  static propTypes = {
    oven: PropTypes.object,
    fetchOvenById: PropTypes.func.isRequired,
    addOvenToBasket: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.fetchOvenById(this.props.match.params.id)
  }

  componentDidUpdate(prevProps, prevState) {
    window.scrollTo(0, 0)
  }

  renderContent = () => {
    const { oven } = this.props

    return (
      <Thumbnail>
        <h2 className="thumbnail__title">{oven.name}</h2>
        <Row className="show-grid">
          <Col md={6}>
            <div style={{ textAlign: 'center' }}>
              <Image
                src={oven.image}
                alt={oven.name}
                style={{ maxHeight: '400px' }}
              />
            </div>
          </Col>
          <Col md={6}>
            <Table striped condensed responsive>
              <tbody>{this.renderFields(oven)}</tbody>
            </Table>
          </Col>
        </Row>

        <p>{oven.description}</p>
        <div className="caption-full">
          <h3 className="oven__price">Цена: {oven.price} руб.</h3>
        </div>
      </Thumbnail>
    )
  }

  /*   renderFields = oven => {
    const renderedFields = R.compose(
      R.toPairs,
      R.pick([
        'fuel',
        'fireboxMetal',
        'color',
        'stones',
        'roomCapacity',
        'chimneyDiameter',
        'firewoodLength',
        'weight',
        'overall'
      ])
    )(oven)

    const newFields = [
      'Вид топлива',
      'Метал топки',
      'Цвет печи',
      'Масса камней',
      'Максимальный объём парилки',
      'Диаметр дымохода',
      'Глубина топки',
      'Вес печи без камней',
      'Габариты (ДхШхВ)'
    ]

    const replacedFields = renderedFields.map((field, i) => {
      field[0] = newFields[i]
      return field
    })

    return replacedFields.map(([key, value]) => (
      <tr className="oven__tr" key={key}>
        <td className="oven__td-title">{key}</td>
        <td className="oven__td-info">{value}</td>
      </tr>
    ))
  } */

  renderFields = oven => {
    const renderedFields = R.compose(
      R.toPairs,
      R.pick([
        'Вид топлива',
        'Метал топки',
        'Цвет печи',
        'Масса камней',
        'Мощность ',
        'Максимальный объём помещения',
        'Диаметр дымохода',
        'Глубина топки',
        'Вес печи',
        'Габариты (ДхШхВ)'
      ])
    )(oven)

    return renderedFields.map(([key, value]) => (
      <tr className="oven__tr" key={key}>
        <td className="oven__td-title">{key}</td>
        <td className="oven__td-info">{value}</td>
      </tr>
    ))
  }

  renderSidebar = () => {
    const { oven, addOvenToBasket } = this.props

    return (
      <div className="cart">
        <BasketCart />
        <h2>{oven.name}</h2>
        <h3>{oven.price} руб.</h3>
        <Button
          bsStyle="success"
          onClick={() => addOvenToBasket(oven.id)}
          block
        >
          Купить
        </Button>
        <Link to="/" className="btn btn-primary btn-block">
          Назад в каталог
        </Link>
      </div>
    )
  }

  render() {
    const { oven } = this.props

    return (
      <Row className="show-grid">
        {oven && <Col md={9}>{this.renderContent()}</Col>}
        {oven && <Col md={3}>{this.renderSidebar()}</Col>}
      </Row>
    )
  }
}

const mapStateToProps = state => ({
  oven: getOvenById(state, state.ovenPage.id)
})

const mapDispatchToProps = { fetchOvenById, addOvenToBasket }

export default connect(mapStateToProps, mapDispatchToProps)(Oven)
