import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Well,
  FormGroup,
  FormControl,
  InputGroup,
  Button,
  Glyphicon
} from 'react-bootstrap'

import { connect } from 'react-redux'
import { searchOven } from 'actions'

import { getSearch } from 'selectors'
class Search extends Component {
  constructor(props) {
    super(props)
    this.state = { value: this.props.search }
  }

  static propTypes = {
    search: PropTypes.string.isRequired,
    searchOven: PropTypes.func.isRequired
  }

  handleChange = e => {
    this.setState({ value: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.searchOven(this.state.value)
  }

  render() {
    return (
      <Well>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <InputGroup>
              <FormControl
                type="text"
                value={this.state.value}
                placeholder="Поиск товара"
                onChange={this.handleChange}
              />
              <InputGroup.Button>
                <Button type="submit">
                  <Glyphicon glyph="search" />
                </Button>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>
        </form>
      </Well>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  search: getSearch(state)
})

const mapDispatchToProps = { searchOven }

export default connect(mapStateToProps, mapDispatchToProps)(Search)
