import React from 'react'

import { BasketCart, Search, Categories } from 'components'
const Sidebar = props => {
  return (
    <div>
      <BasketCart />
      <Search />
      <Categories />
    </div>
  )
}

export default Sidebar
