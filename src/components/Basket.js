import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { isEmpty } from 'ramda'
import {
  Row,
  Col,
  Table,
  Image,
  Button,
  Glyphicon,
  Tooltip,
  OverlayTrigger
} from 'react-bootstrap'

import { getBasketOvensWithCount, getTotalBasketPrice } from 'selectors'
import {
  removeOvenFromBasket,
  cleanBasket,
  basketCheckout,
  addOvenToBasket,
  removeOneOvenFromBasket
} from 'actions'

class Basket extends Component {
  static propTypes = {
    ovens: PropTypes.array.isRequired,
    totalPrice: PropTypes.number.isRequired,
    removeOvenFromBasket: PropTypes.func.isRequired,
    cleanBasket: PropTypes.func.isRequired,
    basketCheckout: PropTypes.func.isRequired,
    addOvenToBasket: PropTypes.func.isRequired,
    removeOneOvenFromBasket: PropTypes.func.isRequired
  }

  renderContent = () => {
    const {
      ovens,
      totalPrice,
      removeOvenFromBasket,
      addOvenToBasket,
      removeOneOvenFromBasket
    } = this.props
    const isBasketEmpty = isEmpty(ovens)

    const tooltip = <Tooltip id="tooltip">удалить</Tooltip>

    return (
      <div className="basket">
        {isBasketEmpty && <h4>Ваша корзина пуста</h4>}
        <Table striped bordered condensed responsive>
          <tbody>
            {ovens.map(oven => (
              <tr key={oven.id}>
                <td className="basket__first-col">
                  <Image src={oven.image} alt={oven.name} thumbnail />
                </td>
                <td>{oven.name}</td>
                <td>{oven.price} руб.</td>
                <td>
                  <Glyphicon
                    glyph="minus"
                    onClick={() => removeOneOvenFromBasket(oven.id)}
                    style={{ cursor: 'pointer' }}
                  />
                  <span className="basket__item-count">{oven.count}</span>
                  <Glyphicon
                    glyph="plus"
                    onClick={() => addOvenToBasket(oven.id)}
                    style={{ cursor: 'pointer' }}
                  />
                </td>
                <td>
                  <OverlayTrigger
                    placement="bottom"
                    overlay={tooltip}
                    delayShow={500}
                  >
                    <Glyphicon
                      glyph="remove"
                      onClick={() => removeOvenFromBasket(oven.id)}
                      style={{ cursor: 'pointer', color: 'tomato' }}
                    />
                  </OverlayTrigger>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        {!isBasketEmpty && (
          <Row>
            <Col md={12}>
              <div className="pull-right basket__total">
                <b>Итого к оплате: {totalPrice} руб.</b>
              </div>
            </Col>
          </Row>
        )}
      </div>
    )
  }

  renderSidebar = () => {
    const { ovens, cleanBasket, basketCheckout } = this.props
    const isBasketEmpty = isEmpty(ovens)

    return (
      <div className="basket-sidebar">
        <Link to="/" className="btn btn-info btn-block">
          <Glyphicon glyph="info-sign" />
          <span>Продолжить покупки</span>
        </Link>

        {!isBasketEmpty && (
          <div>
            <Button bsStyle="danger" block onClick={cleanBasket}>
              <Glyphicon glyph="trash" />
              Очистить корзину
            </Button>
            <Button
              bsStyle="success"
              block
              onClick={() => basketCheckout(ovens)}
            >
              <Glyphicon glyph="envelope" />
              Оформить заказ
            </Button>
          </div>
        )}
      </div>
    )
  }

  render() {
    console.log()
    return (
      <Row className="show-grid">
        <Col md={9}>{this.renderContent()}</Col>
        <Col md={3}>{this.renderSidebar()}</Col>
      </Row>
    )
  }
}

const mapStateToProps = state => ({
  ovens: getBasketOvensWithCount(state),
  totalPrice: getTotalBasketPrice(state)
})

const mapDispatchToProps = {
  removeOvenFromBasket,
  cleanBasket,
  basketCheckout,
  addOvenToBasket,
  removeOneOvenFromBasket
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket)
