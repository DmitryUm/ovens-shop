import React from 'react'

const Loader = () => {
  return (
    <div className="loading-container">
      <div className="spinner" />
      <div className="spinner-center" />
      <div className="loading-text">Загрузка...</div>
    </div>
  )
}

export default Loader
