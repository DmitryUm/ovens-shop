import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Col, Clearfix, Thumbnail, Image, Button } from 'react-bootstrap'
import { take } from 'ramda'

import { fetchOvens, fetchCategories, addOvenToBasket } from 'actions'
import { getOvens, getSearch } from 'selectors'

import { Sidebar, Loader } from 'components'
class Ovens extends Component {
  static propTypes = {
    ovens: PropTypes.array.isRequired,
    fetchOvens: PropTypes.func.isRequired,
    fetchCategories: PropTypes.func.isRequired,
    addOvenToBasket: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.fetchOvens()
    this.props.fetchCategories()
  }

  render() {
    const { ovens, search, addOvenToBasket } = this.props

    if (!ovens.length && !search) return <Loader />

    return (
      <Row className="show-grid">
        <Col md={3}>
          <Sidebar />
        </Col>

        <Col md={9}>
          <Row className="show-grid ovens">
            {ovens.map((oven, index) => (
              <Fragment key={oven.id}>
                <Col sm={4} md={4} lg={4}>
                  <Thumbnail>
                    <div style={{ textAlign: 'center' }}>
                      <Link to={`/oven/${oven.id}`}>
                        <Image src={oven.image} alt={oven.name} />
                      </Link>
                    </div>
                    <h4 className="pull-right">{oven.price} руб.</h4>
                    <h4>
                      <Link to={`/oven/${oven.id}`}>{oven.name}</Link>
                    </h4>
                    <p>{take(55, oven.description)}...</p>
                    <p className="thumbnail__buttons">
                      <Button
                        bsStyle="primary"
                        onClick={() => addOvenToBasket(oven.id)}
                      >
                        В корзину
                      </Button>
                      <Link to={`/oven/${oven.id}`} className="btn btn-default">
                        Подробнее
                      </Link>
                    </p>
                  </Thumbnail>
                </Col>
                {!((index + 1) % 3) && <Clearfix />}
              </Fragment>
            ))}
          </Row>
          {/*           <Row className="show-grid">
            <Col md={12}>
              <Button
                bsStyle="primary"
                className="pull-right"
                onClick={loadMoreOvens}
              >
                Показать больше
              </Button>
            </Col>
          </Row> */}
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  ovens: getOvens(state, ownProps),
  search: getSearch(state)
})

const mapDispatchToProps = {
  fetchOvens,
  fetchCategories,
  addOvenToBasket
}

export default connect(mapStateToProps, mapDispatchToProps)(Ovens)
