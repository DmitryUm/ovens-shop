import React from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { Well, ListGroup } from 'react-bootstrap'

import { getCategories, getActiveCategoryId } from 'selectors'

const Categories = ({ categories, activeCategoryId }) => {
  //или const activeCategoryId = props.match.params.id

  return (
    <Well>
      <h4>Типы печей</h4>
      <ListGroup>
        <Link
          to="/"
          className={`list-group-item${!activeCategoryId ? ' active' : ''}`}
        >
          Все печи
        </Link>
        {categories.map(category => (
          <Link
            to={`/categories/${category.id}`}
            className={`list-group-item${
              activeCategoryId === category.id ? ' active' : ''
            }`}
            key={category.id}
          >
            {category.name}
          </Link>
        ))}
      </ListGroup>
    </Well>
  )
}

const mapStateToProps = (state, ownProps) => ({
  categories: getCategories(state),
  activeCategoryId: getActiveCategoryId(ownProps)
})

export default withRouter(connect(mapStateToProps, null)(Categories))
