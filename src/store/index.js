import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import history from '../history'
import reducer from '../reducers'
import { loadState, saveState } from '../localStorage'

const initialState = loadState()

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)))
)

store.subscribe(() => {
  saveState({
    basket: store.getState().basket,
    ovens: store.getState().ovens
  })
})

window.store = store

export default store
