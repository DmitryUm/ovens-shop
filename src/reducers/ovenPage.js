import * as R from 'ramda'

import { FETCH_OVEN_BY_ID_SUCCESS } from '../constants'

const initialState = {
  id: null
}

const ovenPage = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_OVEN_BY_ID_SUCCESS:
      return R.merge(state, { id: R.prop('id', payload) })
    //or
    //return { ...state, id: payload.id }

    default:
      return state
  }
}

export default ovenPage
