import * as R from 'ramda'
import {
  FETCH_OVENS_SUCCESS,
  LOAD_MORE_OVENS_SUCCESS,
  SEARCH_OVEN
} from '../constants'

const initialState = {
  ids: [],
  search: ''
}

const ovensPage = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_OVENS_SUCCESS:
      return R.merge(state, { ids: R.pluck('id', payload) })

    case LOAD_MORE_OVENS_SUCCESS:
      return R.merge(state, {
        ids: R.concat(state.ids, R.pluck('id', payload))
      })

    case SEARCH_OVEN:
      return R.merge(state, { search: payload })

    default:
      return state
  }
}

export default ovensPage
