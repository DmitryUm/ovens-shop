import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import ovens from './ovens'
import categories from './categories'
import ovensPage from './ovensPage'
import ovenPage from './ovenPage'
import basket from './basket'

const reducer = combineReducers({
  ovens,
  categories,
  ovensPage,
  ovenPage,
  basket,
  router: routerReducer
})

export default reducer
