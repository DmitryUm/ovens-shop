import * as R from 'ramda'

import {
  ADD_OVEN_TO_BASKET,
  REMOVE_OVEN_FROM_BASKET,
  REMOVE_ONE_OVEN_FROM_BASKET,
  CLEAN_BASKET
} from '../constants'

const initialState = []

const basket = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_OVEN_TO_BASKET:
      return R.append(payload, state)

    case REMOVE_OVEN_FROM_BASKET:
      return R.without(R.of(payload), state)

    case REMOVE_ONE_OVEN_FROM_BASKET:
      const index = R.findLastIndex(R.equals(payload))(state)
      return R.remove(index, 1, state)

    case CLEAN_BASKET:
      return []

    default:
      return state
  }
}

export default basket
