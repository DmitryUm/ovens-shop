import * as R from 'ramda'
import { FETCH_CATEGORIES_SUCCESS } from '../constants'

const initialState = {}

const categories = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CATEGORIES_SUCCESS:
      return R.merge(state, R.indexBy(R.prop('id'), payload))

    default:
      return state
  }
}

export default categories
