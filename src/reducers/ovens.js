import * as R from 'ramda'
import {
  FETCH_OVENS_SUCCESS,
  LOAD_MORE_OVENS_SUCCESS,
  FETCH_OVEN_BY_ID_SUCCESS
} from '../constants'

const initialState = {}

const ovens = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_OVENS_SUCCESS:
      return R.merge(state, R.indexBy(R.prop('id'), payload))

    case LOAD_MORE_OVENS_SUCCESS:
      return R.merge(state, R.indexBy(R.prop('id'), payload))

    case FETCH_OVEN_BY_ID_SUCCESS:
      return R.assoc(payload.id, payload, state)
    // or
    //return R.merge(state, { [payload.id]: payload })
    //return { ...state, [payload.id]: payload }

    default:
      return state
  }
}

export default ovens
