import * as R from 'ramda'
import axios from 'axios'

/* import ovens from './ovens'
import categories from './categories'

export const fetchOvens = async () => {
  return new Promise(resolve => {
    resolve(ovens)
  })
}

export const fetchOvenById = async id => {
  return new Promise(resolve => {
    const oven = R.find(R.propEq('id', id), ovens)
    resolve(oven)
  })
}

export const fetchCategories = async () => {
  return new Promise(resolve => {
    resolve(categories)
  })
} */

export const fetchOvens = async () => {
  const { data } = await axios.get(
    'http://www.mocky.io/v2/5a200141310000a61cc0b103'
  )

  return data.ovens
}

export const fetchOvenById = async id => {
  const { data } = await axios.get(
    'http://www.mocky.io/v2/5a200141310000a61cc0b103'
  )

  return R.find(R.propEq('id', id), data.ovens)
}

export const fetchCategories = async () => {
  const { data } = await axios.get(
    'http://www.mocky.io/v2/5a200141310000a61cc0b103'
  )

  return data.categories
}
